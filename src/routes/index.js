import { Route, Routes } from 'react-router-dom';

const routeConfig = (routes) => {
	if (!routes) return <div />;

	return (
		<Routes>
			{routes?.map((route) => (
				<Route key={route.name} path={route.path} element={<route.element />} />
			))}
		</Routes>
	);
};

export default routeConfig;
