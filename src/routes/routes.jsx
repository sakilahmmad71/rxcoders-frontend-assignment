// TODO: simple route contains : name, path, exact, element, protected
// const HomePage = lazy(() => import('../pages/Home/Home'));

import HomePage from '../pages/Home/Home';

const routes = [
	{
		name: 'HomePage',
		path: '/',
		exact: true,
		element: HomePage,
		protected: false,
	},
];

export default routes;
