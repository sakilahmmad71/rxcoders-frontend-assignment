/* eslint-disable react/require-default-props */
import { Box, Button, Divider, Paper, Typography } from '@mui/material';
// eslint-disable-next-line import/no-extraneous-dependencies
import PropTypes from 'prop-types';
import React from 'react';
import Audit from './Audit';

const Audits = ({ audits = [], clearItems = null }) => {
	if (!audits?.length) {
		return null;
	}

	return (
		<Paper elevation={0}>
			<Box sx={{ background: '#f6f6f6', p: 2 }}>
				<Box sx={{ display: 'flex', justifyContent: 'space-between' }} my={1}>
					<Typography variant="h5">Audit Logs : </Typography>

					<Button variant="outlined" color="warning" onClick={clearItems}>
						Clear
					</Button>
				</Box>
				<Divider />

				{audits.map((audit) => (
					<Audit key={audit?.siteId} audit={audit} />
				))}
			</Box>
		</Paper>
	);
};

Audits.propTypes = {
	audits: PropTypes.arrayOf(
		PropTypes.shape({
			siteId: PropTypes.string.isRequired,
			name: PropTypes.string.isRequired,
			city: PropTypes.string.isRequired,
			description: PropTypes.string,
			latitude: PropTypes.number.isRequired,
			longitude: PropTypes.number.isRequired,
			createdAt: PropTypes.string.isRequired,
		})
	).isRequired,
	clearItems: PropTypes.func,
};

export default Audits;
