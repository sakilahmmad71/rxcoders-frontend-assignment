/* eslint-disable react/require-default-props */
/* eslint-disable react/no-unused-prop-types */
import { Box, Paper, Typography } from '@mui/material';
// eslint-disable-next-line import/no-extraneous-dependencies
import PropTypes from 'prop-types';
import React from 'react';

const Audit = ({ audit = {} }) => (
	<Paper elevation={0}>
		<Box p={2} mt={2}>
			<Typography>
				Created By {audit?.name} on {audit?.createdAt}
			</Typography>
		</Box>
	</Paper>
);

Audit.propTypes = {
	siteId: PropTypes.string,
	name: PropTypes.string,
	city: PropTypes.string,
	description: PropTypes.string,
	latitude: PropTypes.number,
	longitude: PropTypes.number,
	createdAt: PropTypes.string,
};

export default Audit;
