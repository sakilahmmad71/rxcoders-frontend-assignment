import CloseIcon from '@mui/icons-material/Close';
import SaveIcon from '@mui/icons-material/Save';
import { Box, Button, CircularProgress, Grid, Paper, Typography } from '@mui/material';
import { Form, Formik } from 'formik';
import { useEffect, useReducer, useState } from 'react';
import ErrorAlert from '../../components/atoms/ErrorAlert';
import TextInputField from '../../components/modecules/TextInputField';
import TeastMessage from '../../components/modecules/ToastMessage';
import useError from '../../hooks/useError';
import { initialState, loadingReducer, startLoading, stopLoading } from '../../reducers/LoadingReducer';
import { sleep } from '../../utils/functions';
import { catchAllErrors } from '../../utils/serializeErrors';
import Audits from './parts/Audits';
import formInitialValues from './validation/initialValues';
import validate from './validation/validate';

const Home = () => {
	const [audits, setAudits] = useState(() => JSON.parse(localStorage.getItem('audits')) || []);
	const [initialValues, setinitialValues] = useState({ ...formInitialValues });
	const [auditSuccessMessage, setAuditSuccessMessage] = useState(false);
	const [loading, dispatch] = useReducer(loadingReducer, initialState);
	const { state: locationError, dispatch: errorDispatcher } = useError();

	const onSubmit = async (data, fn) => {
		const modifiedData = { ...data, createdAt: new Date().toLocaleString() };
		dispatch(startLoading());

		await sleep(2000);
		dispatch(stopLoading());
		setAuditSuccessMessage(true);

		setAudits((prevAudits) => [...prevAudits, modifiedData]);
		localStorage.setItem('audits', JSON.stringify([...audits, modifiedData]));
		fn.resetForm();
	};

	const clearFormData = () => setinitialValues(formInitialValues);

	const clearAuditItems = () => {
		localStorage.removeItem('audits');
		setAudits([]);
	};

	const successGetLocation = (location) => {
		setinitialValues((prevValues) => ({
			...prevValues,
			latitude: location?.coords?.latitude,
			longitude: location?.coords?.longitude,
		}));
	};

	const failedGetLocation = (error) => {
		catchAllErrors(errorDispatcher, error);
	};

	useEffect(() => {
		if (window.navigator.geolocation) {
			window.navigator.geolocation.getCurrentPosition(successGetLocation, failedGetLocation);
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	return (
		<Grid item lg={8} md={8} sm={11} xs={12}>
			<Paper sx={{ p: 4, my: 2 }} elevation={6}>
				<TeastMessage
					message="Audit creates successfully!"
					open={auditSuccessMessage}
					onClose={() => setAuditSuccessMessage(false)}
				/>

				<Box sx={{ display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
					<Typography variant="h4" fontWeight="bold">
						Create An Audit Log
					</Typography>
					<Typography variant="body2">Fill in the fields appropriately and create a log</Typography>
				</Box>

				<Formik initialValues={initialValues} validate={validate} onSubmit={onSubmit} enableReinitialize>
					{() => (
						<Form>
							<fieldset disabled={loading} style={{ border: 'none' }}>
								<Grid container spacing={2} sx={{ py: 4 }}>
									<Grid item xs={12} sx={{ display: 'flex', gap: 2, mb: 2 }}>
										<Button
											type="submit"
											disabled={loading}
											variant="outlined"
											size="large"
											startIcon={<SaveIcon />}
											endIcon={loading && <CircularProgress size={20} />}
										>
											Save
										</Button>

										<Button
											onClick={clearFormData}
											variant="outlined"
											size="large"
											sx={{ color: '#333333', borderColor: '#333333', '&:hover': { borderColor: '#333333' } }}
											startIcon={<CloseIcon />}
										>
											Cancel
										</Button>
									</Grid>

									<Grid item xs={12}>
										<TextInputField fullWidth isRequired label="Site ID" name="siteId" disabled />
									</Grid>

									<Grid item xs={12}>
										<TextInputField fullWidth isRequired label="Name" name="name" />
									</Grid>

									<Grid item xs={12}>
										<TextInputField fullWidth isRequired label="Jurisdiction/City/Region" name="city" />
									</Grid>

									<Grid item xs={12}>
										<TextInputField multiline rows={4} fullWidth label="Site Description" name="description" />
									</Grid>

									{locationError && (
										<Grid item xs={12}>
											<ErrorAlert />
										</Grid>
									)}

									<Grid item lg={6} md={6} sm={6} xs={12}>
										<TextInputField autoFocus fullWidth isRequired label="Latitude" name="latitude" />
									</Grid>

									<Grid item lg={6} md={6} sm={6} xs={12}>
										<TextInputField autoFocus fullWidth isRequired label="Longitude" name="longitude" />
									</Grid>
								</Grid>
							</fieldset>
						</Form>
					)}
				</Formik>

				<Audits audits={audits} clearItems={clearAuditItems} />
			</Paper>
		</Grid>
	);
};

export default Home;
