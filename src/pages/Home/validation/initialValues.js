import { generateRandomNumber } from '../../../utils/functions';

const initialValues = {
	siteId: generateRandomNumber(12),
	name: '',
	city: '',
	description: '',
	latitude: '',
	longitude: '',
};

export default initialValues;
