// eslint-disable-next-line no-unused-vars
const validate = (values) => {
	const errors = {};

	if (!values?.name) {
		errors.name = 'Name is required';
	} else if (!values?.name?.length >= 2) {
		errors.name = 'Name should be at least 2 charecters';
	}

	if (!values?.city) {
		errors.city = 'Jurisdiction/City/Region is required';
	} else if (!values?.city?.length >= 2) {
		errors.city = 'Jurisdiction/City/Region should be at least 2 charecters';
	}

	if (!values?.latitude) {
		errors.latitude = 'Latitude is required';
	}

	if (!values?.longitude) {
		errors.longitude = 'Longitude is required';
	}

	return errors;
};

export default validate;
