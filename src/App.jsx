import { ThemeProvider } from '@mui/material/styles';
import { createBrowserHistory } from 'history';
import { BrowserRouter } from 'react-router-dom';
import BaseLayout from './components/layout/BaseLayout';
import { ErrorProvider } from './contexts/ErrorContext';
import theme from './theme';

const browserHistory = createBrowserHistory();

const App = () => (
	<ThemeProvider theme={theme}>
		<ErrorProvider>
			<BrowserRouter history={browserHistory}>
				<BaseLayout />
			</BrowserRouter>
		</ErrorProvider>
	</ThemeProvider>
);

export default App;
