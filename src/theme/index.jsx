import { createTheme } from '@mui/material/styles';

// TODO: Custom Theming options
const theme = createTheme({
	typography: {
		fontFamily: ['Inter', 'sans-serif'].join(','),
	},
});

export default theme;
