import { AppBar, Box, Toolbar, Typography } from '@mui/material';
import { FOOTER_HEIGHT } from './constants';

const Footer = () => (
	<Box data-testid="rxcoders-footer">
		<AppBar position="static" color="success" sx={{ minHeight: FOOTER_HEIGHT }}>
			<Toolbar sx={{ display: 'flex', justifyContent: 'space-between' }}>
				<Typography variant="body2" data-testid="rxcoders-footer-text">
					&copy; 2022 - RxCoders
				</Typography>

				<Typography variant="body1" data-testid="rxcoders-footer-version">
					Version 1.0.1
				</Typography>
			</Toolbar>
		</AppBar>
	</Box>
);

export default Footer;
