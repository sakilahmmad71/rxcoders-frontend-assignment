import MenuIcon from '@mui/icons-material/Menu';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import IconButton from '@mui/material/IconButton';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import { HEADER_HEIGHT } from './constants';

const Header = () => (
	<Box data-testid="rxcoders-header">
		<AppBar position="static" color="success" sx={{ minHeight: HEADER_HEIGHT }}>
			<Toolbar>
				<Typography variant="h6" component="div" sx={{ flexGrow: 1 }} data-testid="rxcoders-header-text">
					RxCoders.
				</Typography>
				<IconButton size="large" edge="start" color="inherit" aria-label="menu" data-testid="rxcoders-header-icon">
					<MenuIcon />
				</IconButton>
			</Toolbar>
		</AppBar>
	</Box>
);

export default Header;
