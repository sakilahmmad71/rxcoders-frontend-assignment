import { render, screen } from '@testing-library/react';
import Footer from '../Footer';

describe('It Should be contains all the elements and visible in the document', () => {
	it('Should be visible in the document', () => {
		render(<Footer />);

		const footer = screen.getByTestId('rxcoders-footer');
		expect(footer).toBeInTheDocument();
	});

	it('Should be visible footer text in the document', () => {
		render(<Footer />);

		const footer = screen.getByTestId('rxcoders-footer-text');
		expect(footer).toBeInTheDocument();
	});

	it('Should be visible footer version in the document', () => {
		render(<Footer />);

		const footer = screen.getByTestId('rxcoders-footer-version');
		expect(footer).toBeInTheDocument();
	});
});
