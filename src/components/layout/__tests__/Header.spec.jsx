import { render, screen } from '@testing-library/react';
import Header from '../Header';

describe('It Should be contains all the elements and visible in the document', () => {
	it('Should be visible in the document', () => {
		render(<Header />);

		const header = screen.getByTestId('rxcoders-header');
		expect(header).toBeInTheDocument();
	});

	it('Should be visible header text in the document', () => {
		render(<Header />);

		const header = screen.getByTestId('rxcoders-header-text');
		expect(header).toBeInTheDocument();
	});

	it('Should be visible header icon in the document', () => {
		render(<Header />);

		const header = screen.getByTestId('rxcoders-header-icon');
		expect(header).toBeInTheDocument();
	});
});
