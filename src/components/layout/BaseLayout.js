/* eslint-disable no-self-compare */
import { Box } from '@mui/material';
import { Suspense, useState } from 'react';
import routeConfig from '../../routes';
import routes from '../../routes/routes';
import { FOOTER_HEIGHT, HEADER_HEIGHT } from './constants';
import Footer from './Footer';
import Header from './Header';

const BaseLayout = () => {
	// eslint-disable-next-line no-unused-vars
	const [authenticated, setAuthenticated] = useState(false);
	// eslint-disable-next-line no-unused-vars
	const [open, setOpen] = useState(false);

	return (
		<>
			<Header />

			<Box
				sx={{
					minHeight: `calc(100vh - ${HEADER_HEIGHT + FOOTER_HEIGHT}px)`,
					minWidth: '100vw',
					display: 'flex',
					justifyContent: 'center',
					alignItems: 'center',
				}}
			>
				<Suspense fallback={<div>Loading...</div>}>{routeConfig(routes)}</Suspense>
			</Box>

			<Footer />
		</>
	);
};

export default BaseLayout;
