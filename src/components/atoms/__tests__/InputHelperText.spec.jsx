import { render, screen } from '@testing-library/react';
import InputHelperText from '../InputHelperText';

describe('It Should render error text in the document', () => {
	it('Should have text name "error!" and visible in the document', () => {
		render(<InputHelperText error="error!" />);

		const errorText = screen.getByText(/error!/i);

		expect(errorText).toBeInTheDocument();
	});

	it('Should have text name "this-is-an-error" and visible in the document', () => {
		const error = 'This is an error';

		render(<InputHelperText error={error} />);

		const errorText = screen.getByText(/this is an error/i);

		expect(errorText.textContent).toBe(error);
	});
});
