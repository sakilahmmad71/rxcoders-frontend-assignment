/* eslint-disable react/jsx-props-no-spreading */
import MuiAlert from '@mui/material/Alert';
import Snackbar from '@mui/material/Snackbar';
import * as React from 'react';

// eslint-disable-next-line react/jsx-props-no-spreading
const Alert = React.forwardRef((props, ref) => <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />);

const TeastMessage = ({ message = '', ...props }) => (
	<Snackbar autoHideDuration={4000} {...props}>
		<Alert severity="success" sx={{ width: '100%' }} {...props}>
			{message}
		</Alert>
	</Snackbar>
);

export default TeastMessage;
