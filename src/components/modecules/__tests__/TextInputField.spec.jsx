import { render, screen } from '@testing-library/react';
import user from '@testing-library/user-event';
import { Formik } from 'formik';
import TextInputField from '../TextInputField';

describe('It Should tests text input with all validations and errors', () => {
	it('Should render empty input element', () => {
		render(<Formik>{() => <TextInputField name="name" />}</Formik>);

		const input = screen.getByRole('textbox');
		expect(input).toBeInTheDocument();
	});

	it('Should render text input with label "Name"', () => {
		render(<Formik>{() => <TextInputField label="Name" name="name" />}</Formik>);

		const input = screen.getByRole('textbox');
		expect(input).toBeInTheDocument();
	});

	it('Should type "Hello"', () => {
		const greeting = 'Hello';
		render(<Formik initialValues={{ name: '' }}>{() => <TextInputField label="Name" name="name" />}</Formik>);

		const input = screen.getByRole('textbox');
		user.type(input, greeting);
		expect(input.value).toEqual(greeting);
	});
});
