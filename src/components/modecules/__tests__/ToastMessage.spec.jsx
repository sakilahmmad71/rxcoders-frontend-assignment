import { render, screen } from '@testing-library/react';
import ToastMessage from '../ToastMessage';

describe('It Should be in the document for while', () => {
	it('Should be in the document', () => {
		render(<ToastMessage open />);

		const toast = screen.getByRole('alert');
		expect(toast).toBeInTheDocument();
	});
});
