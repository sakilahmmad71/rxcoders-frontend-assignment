import { convertStringToKebabCase, generateRandomNumber } from '../functions';

describe('Should convert normal camelcase string to kebabcase string', () => {
	it('Should convert empty string to ""', () => {
		expect(convertStringToKebabCase()).toBe('');
	});

	it('Should convert "" to ""', () => {
		expect(convertStringToKebabCase('')).toBe('');
	});

	it('Should convert "Rx Coders" to "rx-coders"', () => {
		expect(convertStringToKebabCase('Rx Coders')).toBe('rx-coders');
	});
});

describe('Should generate random number based on given digits', () => {
	it('Should give 8 random numbers if no digit provided', () => {
		expect(generateRandomNumber().toString().length).toEqual(8);
	});

	it('Should give 5 random numbers', () => {
		expect(generateRandomNumber(5).toString().length).toEqual(5);
	});

	it('Should give 12 random numbers', () => {
		expect(generateRandomNumber(12).toString().length).toEqual(12);
	});

	it('Should give "Digits should be number" if given digit is string or NAN', () => {
		expect(generateRandomNumber('1a')).not.toBe(typeof 'number');
	});
});
