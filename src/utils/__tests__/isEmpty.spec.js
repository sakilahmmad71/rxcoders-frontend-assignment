import isEmpty from '../isEmpty';

describe('Should tests input value is empty or not', () => {
	it('Should check empty string variable', () => {
		const name = '';
		expect(isEmpty(name)).toBe(true);
	});

	it('Should check empty undefined variable', () => {
		const name = undefined;
		expect(isEmpty(name)).toBe(true);
	});

	it('Should check empty null variable', () => {
		const name = null;
		expect(isEmpty(name)).toBe(true);
	});

	it('Should check variable is not empty', () => {
		const name = NaN;
		expect(isEmpty(name)).toBe(false);
	});

	it('Should check string variable is not empty', () => {
		const name = 'cityscape';
		expect(isEmpty(name)).toBe(false);
	});

	it('Should check number variable is not empty', () => {
		const name = 786;
		expect(isEmpty(name)).toBe(false);
	});

	it('Should check empty object', () => {
		const obj = {};
		expect(isEmpty(obj)).toBe(true);
	});

	it('Should check empty array', () => {
		const arr = [];
		expect(isEmpty(arr)).toBe(true);
	});

	it('Should check object is not empty', () => {
		const obj = { name: 'Cityscape global limited' };
		expect(isEmpty(obj)).toBe(false);
	});

	it('Should check array is not empty', () => {
		const arr = ['Rx', 'Coders'];
		expect(isEmpty(arr)).toBe(false);
	});
});
