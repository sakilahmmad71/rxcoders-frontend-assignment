export const convertStringToKebabCase = (string = '') => {
	if (string.length === 0) {
		return '';
	}

	return string.toLocaleLowerCase().split(' ').join('-');
};

export const generateRandomNumber = (digit = 8) => {
	const add = 1;
	let max = 12 - add;

	if (typeof parseInt(digit, 10) !== 'number') {
		return 'Digits should be number';
	}

	if (digit > max) {
		return generateRandomNumber(max) + generateRandomNumber(digit - max);
	}

	max = 10 ** (digit + add);
	const min = max / 10;
	const number = Math.floor(Math.random() * (max - min + 1)) + min;

	return `${number}`.substring(add);
};

// eslint-disable-next-line no-promise-executor-return
export const sleep = (ms) => new Promise((resolve) => setTimeout(resolve, ms));
