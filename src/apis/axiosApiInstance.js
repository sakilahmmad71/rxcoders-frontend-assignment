/* eslint-disable no-underscore-dangle */
/* eslint-disable no-param-reassign */
import axios from 'axios';

const axiosApiInstance = axios.create({
	baseURL: '',
	timeout: 1000,
	headers: {
		accept: 'application/json',
	},
});

axiosApiInstance.interceptors.request.use(
	async (config) =>
		// TODO: Apply access token mechanism here

		config,
	(error) => {
		Promise.reject(error);
	}
);

axiosApiInstance.interceptors.response.use(
	(response) => response,
	async (error) => {
		const originalRequest = error.config;

		if ([401, 403].includes(error?.response?.status) && !originalRequest._retry) {
			originalRequest._retry = true;

			// TODO: Apply refresh token mechanism here

			return axiosApiInstance(originalRequest);
		}

		return Promise.reject(error);
	}
);

export default axiosApiInstance;
