import { reducer, types } from '../ErrorReducer';

describe('It Should check error status', () => {
	it('Should error message "error!"', () => {
		expect(reducer('error', { type: types.SHOW_ERROR, payload: 'error' })).toBe('error');
	});

	it('Should error message ""', () => {
		expect(reducer(null, { type: types.REMOVE_ERROR })).toBe(null);
	});
});
