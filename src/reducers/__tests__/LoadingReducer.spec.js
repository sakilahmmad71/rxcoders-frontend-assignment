import { loadingReducer, types } from '../LoadingReducer';

describe('It Should check loading status', () => {
	it('Should loading "true"', () => {
		expect(loadingReducer(true, { type: types.START_LOADING })).toBe(true);
	});

	it('Should loading "false"', () => {
		expect(loadingReducer(false, { type: types.STOP_LOADING })).toBe(false);
	});
});
