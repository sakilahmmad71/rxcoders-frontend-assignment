## RxCoders Assignment Project

[![N|Solid](https://cldup.com/dTxpPi9lDf.thumb.png)](https://nodesource.com/products/nsolid)

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

#### This assignment project for demo purpose of how I structure, reuse, maintain and scale project source code.

## Features

- Simple Form

## Tech
Technologies used for this project

- JavaScript - Programming Language
- React.js - JavaScript Library
- Material UI - UI Library
- Github - Source code management

## Installation

Open your terminal and simply clone this repo.

```sh
git clone https://gitlab.com/sakilahmmad71/rxcoders-frontend-assignment.git
```

Then go to the project directory.

```sh
cd rxcoders-frontend-assignment
```

requires [Node.js](https://nodejs.org/) v12+ to run.

Install the dependencies and devDependencies.

```sh
npm install
```

Check linting of source code.
```sh
npm run lint
```

For run the application.
```sh
npm start
```

Verify the server running by navigating to your server address in
your preferred browser.

```sh
127.0.0.1:3000
or
http://localhost:3000
```
